import React, { FC, useState } from 'react';

import { RouteComponentProps } from '@reach/router';
import { UserCard } from '../components/users/user-card';
import { CircularProgress } from '@mui/material';

import { useLoadingUsers } from '../hooks/useLoadingUsers';
import { Pagination } from '../components/Pagination';

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [page, setPage] = useState(1);
  const { data: users, error, loading } = useLoadingUsers(page);

  if (error) return <h2>Error</h2>;

  return (
    <div style={{ paddingTop: '30px' }}>
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        {loading ? (
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              height: '100vh',
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
          <div>
            {users.length
              ? users.map((user) => {
                  return <UserCard key={user.id} {...user} />;
                })
              : null}
          </div>
        )}
      </div>
      <Pagination page={page} setPage={setPage} />
    </div>
  );
};
