import { useState, useEffect } from 'react';
import { IUserProps } from '../dtos/user.dto';
import { BackendClient } from '../clients/backend.client';

const backendClient = new BackendClient();

export const useLoadingUsers = (page: number) => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const result = await backendClient.getAllUsers(page);
        setUsers(result.data);
        setLoading(false);
      } catch (err) {
        setError(!!err);
      }
    };

    fetchData();
  }, [page]);

  return { data: users, loading, error };
};
