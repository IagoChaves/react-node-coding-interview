import React from 'react';

type PaginationProps = {
  page: number;
  setPage: React.Dispatch<React.SetStateAction<number>>;
};

export const Pagination: React.FC<PaginationProps> = ({ page, setPage }) => {
  const prevPage = () => {
    setPage((previousPage) => previousPage - 1);
  };

  const nextPage = () => {
    setPage((previousPage) => previousPage + 1);
  };

  return (
    <div>
      <button onClick={prevPage} disabled={page === 1}>
        Prev Page
      </button>
      <button onClick={nextPage}>Next Page</button>
    </div>
  );
};
