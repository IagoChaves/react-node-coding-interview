import people_data from '../data/people_data.json';

export class PeopleProcessing {
  getById(id: number) {
    return people_data.find((p) => p.id === id);
  }

  getAll(page: number, offset: number) {
    return people_data.slice((page - 1) * offset, page * offset);
  }
}
